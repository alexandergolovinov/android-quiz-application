package com.alexandergolovinov.quizzler;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    private final static String TRUE = "TRUE";
    private final static String FALSE = "FALSE";

    private Button mTrueButton;
    private Button mFalseButton;
    private ProgressBar mProgressBar;
    private TextView mQuestionTextView;
    private TextView mScore;

    private int mScoreCounter;
    private int mIndex;

    // Question bank
    private TrueFalse[] mQuestionBank = new TrueFalse[]{
            new TrueFalse(R.string.question_1, true),
            new TrueFalse(R.string.question_2, true),
            new TrueFalse(R.string.question_3, true),
            new TrueFalse(R.string.question_4, true),
            new TrueFalse(R.string.question_5, true),
            new TrueFalse(R.string.question_6, false),
            new TrueFalse(R.string.question_7, true),
            new TrueFalse(R.string.question_8, false),
            new TrueFalse(R.string.question_9, true),
            new TrueFalse(R.string.question_10, true),
            new TrueFalse(R.string.question_11, false),
            new TrueFalse(R.string.question_12, false),
            new TrueFalse(R.string.question_13, true)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTrueButton = (Button) findViewById(R.id.true_button);
        mFalseButton = (Button) findViewById(R.id.false_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mQuestionTextView = (TextView) findViewById(R.id.question_text_view);
        mScore = (TextView) findViewById(R.id.score);

        final int mBankLength = mQuestionBank.length;

        if (savedInstanceState != null) {
            mScoreCounter = savedInstanceState.getInt("ScoreKey");
            mIndex = savedInstanceState.getInt("IndexKey");
            updateQuestionText(mIndex, mQuestionBank);
            updateScoreText(mScoreCounter, mBankLength);
            updateProgressBar(mProgressBar, mIndex, mBankLength);
        }

        View.OnClickListener myListener = new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final String btnText = ((Button) view).getText().toString().toUpperCase();
                TrueFalse currentQuestion = mQuestionBank[mIndex];
                mQuestionTextView.setText(currentQuestion.getQuestionID());
                switch (btnText) {
                    case TRUE:
                        performActionLogic(currentQuestion, btnText, mBankLength);
                        break;
                    case FALSE:
                        performActionLogic(currentQuestion, btnText, mBankLength);
                }
            }
        };

        mTrueButton.setOnClickListener(myListener);
        mFalseButton.setOnClickListener(myListener);
    }

    private void performActionLogic(TrueFalse currentQuestion, final String btnText, final int mBankLength) {
        mIndex++;
        if (mIndex == mBankLength) {
            shutDownApplication();
        } else {
            checkAnswerAndIncrement(currentQuestion, btnText, mBankLength);
            updateProgressBar(mProgressBar, mIndex, mBankLength);
            updateQuestionText(mIndex, mQuestionBank);
        }
    }

    private int updateScore() {
        return ++mScoreCounter;
    }

    //Check answer
    private void checkAnswerAndIncrement(TrueFalse mTrueFalseQuestion, final String mBtnText, int mBankLength) {
        boolean answer = mTrueFalseQuestion.getAnswer();
        if (answer == Boolean.valueOf(mBtnText)) {
            updateScoreText(updateScore(), mBankLength);
            showToast(TRUE);
        } else {
            showToast(FALSE);
        }
    }

    private void showToast(final String text) {
        Toast.makeText(this, text, (int) 0.02).show();
    }

    private void updateScoreText(final int mScoreCounter, final int mMaxArraySize) {
        final String scoreText = "Score " + mScoreCounter + "/" + mMaxArraySize;
        mScore.setText(scoreText);


    }

    private void updateProgressBar(final ProgressBar progressBar, final int mIndex, final int mMaxArraySize) {
        progressBar.setMax(mMaxArraySize);
        progressBar.setProgress(mIndex);
    }

    private AlertDialog shutDownApplication() {
        return new AlertDialog.Builder(this)
                .setTitle("Game over")
                .setCancelable(false)
                .setMessage("You scored: " + mScoreCounter + " points")
                .setNegativeButton("Start over", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startAgain();
                    }
                })
                .setPositiveButton("Close Application", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();

    }

    private void updateQuestionText(final int mIndex, TrueFalse... mQuestionBank) {
        mQuestionTextView.setText(mQuestionBank[mIndex].getQuestionID());
    }

    private void startAgain() {
        mScoreCounter = 0;
        mIndex = 0;
        updateScoreText(mScoreCounter, mQuestionBank.length);
        updateProgressBar(mProgressBar, mIndex, mQuestionBank.length);
        updateQuestionText(mIndex, mQuestionBank);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("ScoreKey", mScoreCounter);
        outState.putInt("IndexKey", mIndex);

    }
}
