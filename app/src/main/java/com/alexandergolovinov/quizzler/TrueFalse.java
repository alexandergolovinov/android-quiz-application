package com.alexandergolovinov.quizzler;

public class TrueFalse {

    private int mqQuestionID;
    private boolean mAnswer;

    public TrueFalse(int mqQuestionID, boolean answer) {
        this.mqQuestionID = mqQuestionID;
        this.mAnswer = answer;
    }

    public int getQuestionID() {
        return mqQuestionID;
    }

    public void setQuestionID(int mqQuestionID) {
        this.mqQuestionID = mqQuestionID;
    }

    public boolean getAnswer() {
        return mAnswer;
    }
}
